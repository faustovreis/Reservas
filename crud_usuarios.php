<?php 
include_once './verificar.php';
require_once 'Classes/Usuarios.php';
?> 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Agenda</title>
        <!-- Bootstrap -->
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

        <!-- CSS SESSION -->
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
        <link href="assets/bootstrap-3.3.7/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="assets/extras/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <link href="assets/bootstrap-td_1.11.1/dist/bootstrap-table.css" type="text/css" rel="stylesheet"> 
        <link href="assets/jquery/jquery_dragtable_2.0.10/dragtable.css" type="text/css" rel="stylesheet" > 
        <link href="assets/jquery/jquery-ui-1.12.1/jquery-ui.css" type="text/css" rel="stylesheet" > 
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

        <!-- JS SESSION -->
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
        <script type="text/javascript" src="assets/jquery/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/bootstrap-table.js"></script>
        <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/locale/bootstrap-table-pt-BR.js"></script>
        <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/reorder-columns/bootstrap-table-reorder-columns.js"></script>
        <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/toolbar/bootstrap-table-toolbar.js"></script>
        <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/bootstrap-table-resizable.js"></script>
        <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/colResizable-1.5.source.js"></script>
        <script type="text/javascript" src="assets/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="assets/jquery/jquery_dragtable_2.0.10/jquery.dragtable.js"></script>
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
 </head>

<body>
    <?php
    include "menu.php";
    ?>
    <div class="container">
        <br><br>
        <h1>Cadastro de Usuários</h1>
        <?php 
        $usuario = new Usuarios();
        if (@$_POST['btn']=="Salvar") {
            @$usuario->setNome($_POST['nome']);
            @$usuario->setEmail($_POST['email']);
            @$usuario->setSenha1($_POST['senha1']);
            @$usuario->setSenha2($_POST['senha2']);
            
            if ($_REQUEST['acao']=="Editar") {
                if ($usuario->update($_POST['id'])){
                    echo '<div class="alert alert-success">
                          <strong>Usuário atualizado com sucesso!</strong>
                          </div>'; 
                    echo '<meta http-equiv="refresh" content="5; url=listaUsuarios.php">';
                    exit;
                }
            } else if ($_REQUEST['acao']=="Senha") {
                if ($_POST['senha1']!=$_POST['senha2']) {
                    echo '<div class="alert alert-warning">
                          <strong>Atenção!</strong> Senhas não conferem!.
                          </div>';
                } else {               
                    if ($usuario->senha($_POST['id'])){
                        echo '<div class="alert alert-success">
                              <strong>Senha alterada com sucesso</strong>
                              </div>';       
                        echo '<meta http-equiv="refresh" content="5; url=listaUsuarios.php">';
                        exit;
                    }
                }            
            }else {
                if ($_POST['senha1']!=$_POST['senha2']) {
                    echo '<div class="alert alert-warning">
                          <strong>Atenção!</strong> Senhas não conferem!.
                          </div>';
                } else {
                    if ($usuario->insert()){
                        echo '<div class="alert alert-success">
                              <strong>Usuário adicionado com sucesso!</strong>
                              </div>';       
                        unset($_POST['nome']);
                        unset($_POST['email']);
                        unset($_POST['senha1']);
                        unset($_POST['senha2']);
                    }
                }               
            }
        } else if ($_GET['acao']!="Adicionar") {
            $id = (int)$_GET['id'];
            $resultado=$usuario->find($id);
            $_POST['nome']=$resultado->nome;
            $_POST['email']=$resultado->email;
        }
        ?>
        <form method="post">
            <div class="jumbotron">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $_GET['acao']; ?> - Dados do usuário</div>
                    <div class="panel-body">
                        <?php
                        if ($_GET['acao']!="Senha") {
                        ?>
                        <div class="row">
                            <div class="col-lg-4">
                                <labeL>Nome:</labeL>
                                <input type="text" name="nome"  id="nome" class="form-control" value="<?php echo empty($_POST['nome'])?"":$_POST['nome']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <labeL>E-mail:</labeL>
                                <input type="text" name="email"  id="email" class="form-control" value="<?php echo empty($_POST['email'])?"":$_POST['email']; ?>" >
                            </div>
                        </div>
                        <?php
                        }
                        if ($_GET['acao']=="Adicionar" || $_GET['acao']=="Senha") { 
                        ?>
                        <div class="row">
                            <div class="col-lg-2">
                                <labeL>Senha:</labeL>
                                <input type="password" name="senha1"  id="senha1" class="form-control" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <labeL>Confirmação de senha:</labeL>
                                <input type="password" name="senha2"  id="senha1" class="form-control" >
                            </div>
                        </div>
                        <?php } ?>                        
                        <br>
                        <div class="row">
                            <div class="col-lg-3">
                                <input type="submit" name="btn" value="Salvar" id="btn" class="btn btn-primary">
                                <a href="listaUsuarios.php" class="btn btn-danger">Cancelar</a>
                                <input type="hidden" name="acao" value="<?php echo @$_GET['acao']; ?>" >
                                <input type="hidden" name="id" value="<?php echo @$_GET['id']; ?>" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>
</body>
</html>