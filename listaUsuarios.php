<?php
include_once './verificar.php';
require_once 'Classes/Usuarios.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Agenda</title>
    <!-- Bootstrap -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    <!-- CSS SESSION -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <link href="assets/bootstrap-3.3.7/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="assets/extras/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="assets/bootstrap-td_1.11.1/dist/bootstrap-table.css" type="text/css" rel="stylesheet"> 
    <link href="assets/jquery/jquery_dragtable_2.0.10/dragtable.css" type="text/css" rel="stylesheet" > 
    <link href="assets/jquery/jquery-ui-1.12.1/jquery-ui.css" type="text/css" rel="stylesheet" > 
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    <!-- JS SESSION -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <script type="text/javascript" src="assets/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/bootstrap-table.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/locale/bootstrap-table-pt-BR.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/reorder-columns/bootstrap-table-reorder-columns.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/toolbar/bootstrap-table-toolbar.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/bootstrap-table-resizable.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/colResizable-1.5.source.js"></script>
    <script type="text/javascript" src="assets/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/jquery/jquery_dragtable_2.0.10/jquery.dragtable.js"></script>
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
</head>
<body>
    
    <?php
    include "menu.php";
    ?>
    <div class="top-space">
    	<div class="container">
            <br><br><br>
            <?php
            $usuario = new Usuarios();
            if (isset($_GET['acao']) && $_GET['acao']=="Excluir"){
                $id = (int)$_GET['id'];
                try {
                    $usuario->delete($_GET['id']);
                } catch (Exception $ex) {
                    echo '<div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong> Falha ao excluir a usuário.</strong>
                        </div>';  
                }
        
            }
            ?>
            <h3 class="widget-title">Cadastro de usuários</h3>  
            <div id="toolbar" style="text-align:right">
                <a  href="crud_usuarios.php?acao=Adicionar">
                <button type="button" class="btn btn-info">
                    <i class="glyphicon glyphicon-plus"></i> Adicionar usuário
                </button>
                </a>
            </div>
            <table id="table" 
                    data-toggle="table"
                    data-show-columns="true" 
                    data-search="true" 
                    data-show-toggle="true"
                    data-pagination="true"
                    data-reorderable-columns="true"
                    data-show-pagination-switch="true"
                    data-page-size="10"
                    data-page-list="[10, 25]"
                    data-resizable="true"
                    data-toolbar="#toolbar" style="background-color: #ffffff">
                    <thead>
                        <tr>
                            <th  data-field="ID" data-sortable="true">Id</th>
                            <th  data-field="Nome" data-sortable="true">Nome</th>
                            <th  data-field="Email" data-sortable="true">E-mail</th>
                            <th  data-field="Acoes" data-sortable="false"  data-switchable="false">Ações</th>
			</tr>
                    </thead>
                    <tbody>
                        <?php
                          foreach ($usuario->findall() as $key => $value):
                        ?>
			<tr>
                            <td><?php echo $value->id; ?></td>
                            <td><?php echo $value->nome; ?></td>
                            <td><?php echo $value->email; ?></td>
                            <td><center>
                                <a href="crud_usuarios.php?id=<?php echo $value->id; ?>&acao=Editar" title="Editar usuario">
                                    <i class="fa  fa-edit fa-2x"></i>
                                </a>
                                | 
                                <a href="listaUsuarios.php?id=<?php echo $value->id; ?>&acao=Excluir" title="Excluir usuario" onclick="return confirm('Deseja realmente excluir o usuário?')">
                                    <i class="fa fa-trash fa-2x"></i>
                                </a>
                                | 
                                <a href="crud_usuarios.php?id=<?php echo $value->id; ?>&acao=Senha" title="Trocar senha">
                                    <i class="fa fa-lock fa-2x"></i>
                                </a>
                                </center>
                            </td>                    
			</tr>
                        <?php
                        endforeach;
                        ?>			
                    </tbody>
            </table>	
        </div>
    </div>
</body>
</html>