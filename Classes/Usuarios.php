<?php
/**
 * Description of Usuarios
 *
 * @author Fausto Reis
 */
require_once 'Classes/crud.php';

class Usuarios extends crud {
    protected $table = "usuarios";
    private $nome;
    private $email;
    private $senha1;
    private $senha2;
    private $id;
    
    public function setId($id) {
        $this->id = $id;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setSenha1($senha1) {
        $this->senha1 = $senha1;
    }

    public function setSenha2($senha2) {
        $this->senha2 = $senha2;
    }
    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getId() {
        return $this->id;
    }

    public function insert() {
        $sql = "INSERT INTO  $this->table  (nome, email, senha) VALUES (:nome, :email, :senha1)";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":senha1", $this->senha1);
        return $stmt->execute();
    }

    public function update($id) {
        $sql = "UPDATE $this->table SET nome = :nome, email = :email WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":id", $id);
        return $stmt->execute();
    }
    
    public function senha($id) {
        $sql = "UPDATE $this->table SET senha = :senha WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":senha", $this->senha1);
        $stmt->bindParam(":id", $id);
        return $stmt->execute();
    }
    
    public function findEmail($email, $senha) {
        $sql = "SELECT * FROM $this->table WHERE email = :email";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":email", $email);
        $stmt->execute();
        $rs=$stmt->fetch();
        if ($rs) {
            $this->nome = $rs->nome;        
            $this->email = $rs->email;
            $this->id = $rs->id;
            if ($rs->email == $email) {
                
                if ($rs->senha == $senha) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            $this->nome = null;        
            $this->email = null;
            $this->id = null;
            return false;
        }
    }
}
