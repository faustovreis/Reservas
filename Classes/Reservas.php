<?php
/**
 * Description of Reservas
 *
 * @author Fausto Reis
 */
require_once 'Classes/DB.php';

class Reservas extends DB {
    protected $table = "reservas";
    private $id_usuario;
    private $id_sala;
    private $data;
    private $hora;
    function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    function setId_sala($id_sala) {
        $this->id_sala = $id_sala;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setHora($hora) {
        $this->hora = $hora;
    }

        public function insert() {
        $sql = "INSERT INTO  $this->table  (id_usuario, id_sala, data, hora) VALUES (:id_usuario, :id_sala, :data, :hora)";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":id_usuario", $this->id_usuario);
        $stmt->bindParam(":id_sala", $this->id_sala);
        $stmt->bindParam(":data", $this->data);
        $stmt->bindParam(":hora", $this->hora);
        return $stmt->execute();
    }
    
    public function listSalas($dt,$id_sala) {
        $sql = "SELECT hor.horario, 
        (SELECT res.data FROM reservas res WHERE res.data=:dt AND res.id_sala=:id_sala AND res.hora=hor.horario) AS DATA,
        (SELECT usu.nome FROM reservas res INNER JOIN usuarios usu ON res.id_usuario=usu.id WHERE res.data=:dt AND res.id_sala=:id_sala AND res.hora=hor.horario) AS nome,
        (SELECT sal.descricao FROM reservas res INNER JOIN salas sal ON res.id_sala=sal.id WHERE res.data=:dt AND res.id_sala=:id_sala AND res.hora=hor.horario) AS descricao,
        (SELECT res.id FROM reservas res INNER JOIN salas sal ON res.id_sala=sal.id WHERE res.data=:dt AND res.id_sala=:id_sala AND res.hora=hor.horario) AS id,
        (SELECT res.id_usuario FROM reservas res INNER JOIN salas sal ON res.id_sala=sal.id WHERE res.data=:dt AND res.id_sala=:id_sala AND res.hora=hor.horario) AS id_usuario,
        (SELECT 'Ocupada' FROM reservas res INNER JOIN usuarios usu ON res.id_usuario=usu.id WHERE res.data=:dt AND res.id_sala=:id_sala AND res.hora=hor.horario) AS situacao
        FROM horarios  hor
        ";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":dt", $dt);
        $stmt->bindParam(":id_sala", $id_sala);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function delete($id) {
        $sql = "DELETE FROM $this->table WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        return $stmt->execute();        
    }
}
