<?php
/**
 * Description of Salas
 *
 * @author Fausto Reis
 */
require_once 'Classes/crud.php';

class Salas extends crud {
    protected $table = "salas";
    private $descricao;
    private $id;
    
    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setId($id) {
        $this->id = $id;
    }

    public function insert() {
        $sql = "INSERT INTO  $this->table  (descricao) VALUES (:descricao)";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":descricao", $this->descricao);
        return $stmt->execute();
    }

    public function update($id) {
        $sql = "UPDATE $this->table SET descricao = :descricao WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":descricao", $this->descricao);
        $stmt->bindParam(":id", $id);
        return $stmt->execute();
    }
}
