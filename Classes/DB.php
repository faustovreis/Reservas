<?php
/**
 * Description of DB
 *
 * @author Fausto Reis
 */
class DB {
    private static $instance;
    
    public static function getInstance(){
        if (!isset(self::$instace)){
            try {
                
                self::$instance = new PDO("mysql:dbname=banco;host=localhost","root","");
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);   
                
            } catch (Exception $ex) {
                echo $ex->getCode();
            }
        }
        return self::$instance;
    }
    
    public static function prepare($sql){
        return self::getInstance()->prepare($sql);
    }
}
