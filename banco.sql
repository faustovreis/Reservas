/*
SQLyog Ultimate v9.51 
MySQL - 5.6.17 : Database - banco
*********************************************************************
*/

CREATE DATABASE `banco`;

USE `banco`;

/*Table structure for table `horarios` */

DROP TABLE IF EXISTS `horarios`;

CREATE TABLE `horarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do horario',
  `horario` TIME DEFAULT NULL COMMENT 'Horarios',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `horarios` */

INSERT  INTO `horarios`(`id`,`horario`) VALUES (1,'08:00:00'),(2,'09:00:00'),(3,'10:00:00'),(4,'11:00:00'),(5,'12:00:00'),(6,'13:00:00'),(7,'14:00:00'),(8,'15:00:00'),(9,'16:00:00'),(10,'17:00:00'),(11,'18:00:00'),(12,'19:00:00');

/*Table structure for table `salas` */

DROP TABLE IF EXISTS `salas`;

CREATE TABLE `salas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Código da sala',
  `descricao` VARCHAR(20) DEFAULT NULL COMMENT 'Identificação da sala',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `salas` */

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do usuário',
  `nome` VARCHAR(50) DEFAULT NULL COMMENT 'Nome do usuário',
  `email` VARCHAR(150) DEFAULT NULL COMMENT 'E-mail do usuário',
  `senha` VARCHAR(10) DEFAULT NULL COMMENT 'Senha do usuário',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `usuarios` */

INSERT  INTO `usuarios`(`id`,`nome`,`email`,`senha`) VALUES (1,'Fausto vilanova Reis','fausto.reis@gmail.com','1234');

/*Table structure for table `reservas` */

DROP TABLE IF EXISTS `reservas`;

CREATE TABLE `reservas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Código da reserva',
  `id_usuario` INT(11) DEFAULT NULL COMMENT 'Código do usuário que fez a reserva',
  `id_sala` INT(11) DEFAULT NULL COMMENT 'Código da sala reservada',
  `data` DATE DEFAULT NULL COMMENT 'Data da reserva',
  `hora` TIME DEFAULT NULL COMMENT 'Hora da reserva',
  PRIMARY KEY (`id`),
  KEY `FK_usuario` (`id_usuario`),
  KEY `FK_sala` (`id_sala`),
  CONSTRAINT `FK_sala` FOREIGN KEY (`id_sala`) REFERENCES `salas` (`id`),
  CONSTRAINT `FK_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `reservas` */

