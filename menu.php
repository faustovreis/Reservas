<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="principal.php"><img alt="Sistema de reserva da salas "></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li class="active"><a href="principal.php">Agenda</a></li>
                <li><a href="listaUsuarios.php">Usuários</a></li>
                <li><a href="listaSalas.php">Salas</a></li>
                <li><a class="btn" href="sair.php">Sair</a></li>
            </ul>
        </div>
    </div>
</div> 