<?php
session_start();
if (empty($_SESSION["userName"]) && empty($_SESSION['userEmail'])) {
    echo '<meta http-equiv="refresh" content="0; url=index.php">';
    exit;
}

