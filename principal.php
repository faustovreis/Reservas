<?php
include_once './verificar.php';
require_once 'Classes/Salas.php';
require_once 'Classes/Reservas.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Agenda</title>
    <!-- Bootstrap -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    <!-- CSS SESSION -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <link href="assets/bootstrap-3.3.7/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="assets/extras/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="assets/bootstrap-td_1.11.1/dist/bootstrap-table.css" type="text/css" rel="stylesheet"> 
    <link href="assets/jquery/jquery_dragtable_2.0.10/dragtable.css" type="text/css" rel="stylesheet" > 
    <link href="assets/jquery/jquery-ui-1.12.1/jquery-ui.css" type="text/css" rel="stylesheet" > 
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    <!-- JS SESSION -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <script type="text/javascript" src="assets/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/bootstrap-table.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/locale/bootstrap-table-pt-BR.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/reorder-columns/bootstrap-table-reorder-columns.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/toolbar/bootstrap-table-toolbar.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/bootstrap-table-resizable.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/colResizable-1.5.source.js"></script>
    <script type="text/javascript" src="assets/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/jquery/jquery_dragtable_2.0.10/jquery.dragtable.js"></script>
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
</head>
<body>
    <?php
    include "menu.php";
    ?>
    <div class="top-space">
        
    	<div class="container">
        <br><br><br>
        <form method="post" class="form-group">
            <div class="row">
                <div class="col-lg-4">
                    <label>Reservas do dia:</label> 
                    <input type="date" name="dt" class="form-control">
                </div>
                <div class="col-lg-4">
                    <label>Sala:</label> 
                    <select  class="form-control" name="idSala">
                    <?php
                    $salas = new Salas();
                    foreach ($salas->findall() as $key =>$value): ?> 
                    <option value="<?php echo $value->id; ?>"><?php echo $value->descricao; ?></option>
                    <?php
                    endforeach;
                    ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <br>
                    <input type="submit" value="Pesquisar" class="btn btn-info" name="btn">                
                </div>
            </div>
        </form>   
            <?php
            if (!empty($_REQUEST['idSala']) && !empty($_REQUEST['dt'])) { 
                $reservas = new Reservas();
                if (@$_GET['acao']=="Excluir") {
                    if (@$_GET['id_usuario']==$_SESSION['userId']) {
                        $reservas->delete($_REQUEST['id']);                        
                    }else {
                         echo '<div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Não é possivel desocupar a sala.</strong>
                        </div>';  
                    }
                } else if (@$_GET['acao']=="Adicionar") {
                    $reservas->setData($_GET['dt']);
                    $reservas->setHora($_GET['hora']);
                    $reservas->setId_sala($_GET['idSala']);
                    $reservas->setId_usuario($_SESSION['userId']);
                    $reservas->insert();
                }
                $idSala = $_REQUEST['idSala'];
                $dt = $_REQUEST['dt'];
                $data = new DateTime($dt);
                $local = $salas->find($idSala);
                ?>
                <div id="toolbar" style="text-align:right">
                    <h3>Ocupação da sala <?php echo $local->descricao; ?> para dia <?php echo $data->format('d/m/Y'); ?></h3>               
                </div>
                <table id="table" 
                    data-toggle="table"
                    data-show-columns="false" 
                    data-search="false" 
                    data-show-toggle="false"
                    data-pagination="false"
                    data-reorderable-columns="true"
                    data-show-pagination-switch="false"
                    data-page-size="10"
                    data-page-list="[10, 25]"
                    data-resizable="true"
                    data-toolbar="#toolbar" style="background-color: #ffffff">
                    <thead>
                        <tr>
                            <th  data-field="Codigo" data-sortable="true">Horário</th>
                            <th  data-field="Usuário" data-sortable="true">Sala</th>
                            <th  data-field="Horario" data-sortable="true">Usuário</th>
                            <th  data-field="situacao" data-sortable="false"  data-switchable="false">Situacao</th>
                            <th  data-field="Acoes" data-sortable="false"  data-switchable="false">Menu</th>
			</tr>
                    </thead>
                    <tbody>
			<?php                         
                          foreach ($reservas->listSalas($dt,$idSala) as $key => $value):
                        ?><tr>
                            
                            <td><?php echo $value->horario; ?></td>
                            <td><?php echo $value->descricao; ?></td>
                            <td><?php echo $value->nome; ?></td>
                            <td><center><?php echo (empty($value->situacao))?"livre":$value->situacao; ?></center></td>
                            <td>
                                <center><?php
                                    if (empty($value->situacao) ) {
                                        echo '<a onclick="return confirm(\'Deseja ocupar a sala?\')" href="principal.php?acao=Adicionar&dt='.$dt.'&idSala='.$idSala.'&hora='.$value->horario.'">';
                                        echo '<input type="button" class="btn btn-info" value="Marcar">';
                                        echo '</a>';
                                    } else {
                                        echo '<a onclick="return confirm(\'Deseja desmarcar a ocupação da sala?\')" href="principal.php?acao=Excluir&id='.$value->id.'&dt='.$dt.'&idSala='.$idSala.'&id_usuario='.$value->id_usuario.'">';
                                        echo '<input type="button" class="btn btn-danger" value="Desmarcar">';
                                        echo '</a>';
                                    }
                                    ?>
                                </center>
                            </td>
			</tr>
                        <?php
                        endforeach;
                        ?>	
                    </tbody>
		</table>	
        <br>
        <br>
            <?php
            }
            ?>
        </div>
    </div>
</body>
</html>