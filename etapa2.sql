SELECT dep.dept_name AS Departamento, CONCAT(emp.first_name," ",emp.last_name) AS Funcionario,  DATEDIFF(de.to_date,de.from_date) AS Dias
FROM employees emp
INNER JOIN dept_emp de
ON emp.emp_no=de.emp_no
INNER JOIN departments dep 
ON de.dept_no=dep.dept_no
ORDER BY dias DESC LIMIT 0,10 

