<?php
session_start();
require_once 'Classes/Usuarios.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Agenda</title>
    <!-- Bootstrap -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    <!-- CSS SESSION -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <link href="assets/bootstrap-3.3.7/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="assets/extras/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="assets/bootstrap-td_1.11.1/dist/bootstrap-table.css" type="text/css" rel="stylesheet"> 
    <link href="assets/jquery/jquery_dragtable_2.0.10/dragtable.css" type="text/css" rel="stylesheet" > 
    <link href="assets/jquery/jquery-ui-1.12.1/jquery-ui.css" type="text/css" rel="stylesheet" > 
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    <!-- JS SESSION -->
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <script type="text/javascript" src="assets/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/bootstrap-table.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/locale/bootstrap-table-pt-BR.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/reorder-columns/bootstrap-table-reorder-columns.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/toolbar/bootstrap-table-toolbar.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/bootstrap-table-resizable.js"></script>
    <script type="text/javascript" src="assets/bootstrap-td_1.11.1/dist/extensions/resizable/colResizable-1.5.source.js"></script>
    <script type="text/javascript" src="assets/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/jquery/jquery_dragtable_2.0.10/jquery.dragtable.js"></script>
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
</head>
<body>
    <header id="head" class="secondary"></header>
	<div class="container">
            <div class="row">
            <article class="col-xs-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Sistema de reserva da salas</h1>
                </header>
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="thin text-center">Faça login em sua conta</h3>
                            <hr>
                            <form method="post">
                                <div class="top-margin">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="email" required="required">
				</div>
                                <br>
				<div class="top-margin">
                                    <label>Senha <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="senha" required="required">
				</div>
				<hr>
				<div class="row">									
                                    <div class="col-lg-4">
                                        <button class="btn btn-action" type="submit">Entrar</button>
                                    </div>
                                </div>
                            </form>
                            <?php
                            if($_SERVER['REQUEST_METHOD']=='POST')
                                {
                                $usuario = new Usuarios();
                                if ($usuario->findEmail(@$_POST['email'], @$_POST['senha'])) {
                                    $_SESSION['userName']  = $usuario->getNome();
                                    $_SESSION['userEmail'] = $usuario->getEmail();
                                    $_SESSION['userId'] = $usuario->getId();
                                    echo '<meta http-equiv="refresh" content="0; url=principal.php">';
                                    exit;
                                } else {
                                    echo '<br><div class="alert alert-warning">
                                          <strong>Atenção!</strong> Falha no login!.
                                          </div>';
                                }
                            }
                            ?>
			</div>
                    </div>
		</div>
            </article>
            </div>
	</div>
</body>
</html>